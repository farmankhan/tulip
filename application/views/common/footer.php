<!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- base:js -->
    <script src="<?php echo $this->config->item('vendors_assets_url');?>base/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="<?php echo $this->config->item('js_url');?>template.js"></script>
    <!-- endinject -->
    <!-- plugin js for this page -->
    <!-- End plugin js for this page -->
    <script src="<?php echo $this->config->item('vendors_assets_url');?>chart.js/Chart.min.js"></script>
    <script src="<?php echo $this->config->item('vendors_assets_url');?>progressbar.js/progressbar.min.js"></script>
		<script src="<?php echo $this->config->item('vendors_assets_url');?>chartjs-plugin-datalabels/chartjs-plugin-datalabels.js"></script>
		<script src="<?php echo $this->config->item('vendors_assets_url');?>justgage/raphael-2.1.4.min.js"></script>
		<script src="<?php echo $this->config->item('vendors_assets_url');?>justgage/justgage.js"></script>
    <!-- Custom js for this page-->
    <script src="<?php echo $this->config->item('js_url');?>dashboard.js"></script>
    <!-- End custom js for this page-->
  </body>
</html>