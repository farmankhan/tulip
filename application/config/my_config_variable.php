<?php

$config['css_url'] = config_item('base_url').'assets/css/';

$config['js_url'] = config_item('base_url').'assets/js/';

$config['image_url'] = config_item('base_url').'assets/images/';

$config['fonts_css_url'] = config_item('base_url').'assets/fonts/';

$config['vendors_assets_url'] = config_item('base_url').'assets/vendors/';