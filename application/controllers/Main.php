<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct() {
        
        parent::__construct();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('url', 'language'));
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->lang->load('auth');
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        
        $data['counts'] = null;//$this->offre_model->count();
        $this->makePage('dashboard',$data);
	}


	public function logout()
	{
		$this->ion_auth->logout();
	}

	public function debts()
	{
	 	if (!$this->ion_auth->logged_in()) {
	        redirect('auth/login', 'refresh');
	    }
	    $data['dashboardData'] = null;//$this->offre_model->count();
        $this->makePage('debts',$data);
	}

	public function salahTracker()
	{
	 	if (!$this->ion_auth->logged_in()) {
	        redirect('auth/login', 'refresh');
	    }
	    $data['dashboardData'] = null;//$this->offre_model->count();
        $this->makePage('salahDashboard',$data);
	}

	public function expenseTracker()
	{
	 	if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $data['dashboardData'] = null;//$this->offre_model->count();
        $this->makePage('expenseDashboard',$data);
	}

	public function settings(){
		if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $data['dashboardData'] = null;//$this->offre_model->count();
        $this->makePage('settings',$data);	
	}

	public function passwordManager()
	{
		if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $data['passwordData'] = null;//$this->offre_model->count();
        $this->makePage('passwordDashboard',$data);
	}
	 

	private function makePage($template, $arr = null){
      $this->load->view('common/header');
      $this->load->view($template, $arr);
      $this->load->view('common/footer');
	}
}